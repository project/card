<?php

/**
 * @file
 * Hook implementations and general functions.
 */

/**
 * Implements hook_menu().
 */
function card_menu() {
  $items['admin/config/services/payment/billing'] = array(
    'page callback' => 'drupal_get_form',
    'page arguments' => array('card_form_configuration'),
    'access arguments' => array('card.administer'),
    'title' => 'Billing',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function card_permission() {
  return array(
    'card.administer' => array(
      'title' => t('Administer Billing'),
    ),
  );
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function card_form_field_ui_field_edit_form_alter(array &$form, array &$form_state) {
  // Make sure that users cannot un-require fields needed for billing
  // information.
  foreach (card_field_dependencies() as $name => $dependency) {
    if ($form['#field']['field_name'] == $dependency['field_name']) {
      $form['instance']['required']['#disabled'] = $form['instance']['widget']['settings']['format_handlers']['#disabled'] = TRUE;
      $form['instance']['required']['#description'] = $form['instance']['widget']['settings']['format_handlers']['#description'] = t('This setting is controlled by <a href="@url">Billing</a>.', array(
        '@url' => url('admin/config/services/payment/billing'),
      ));
    }
  }
}

/**
 * Implements hook_help().
 */
function card_help($path, $arg) {
  switch ($path) {
    case 'admin/config/services/payment/billing';
      return '<p>' . t('Select the fields to use for billing information. Some billing services will only use the first field value. Billing will automatically assume control of selected fields.') . '</p>';
  }
}

/**
 * Implements hook_field_attach_form().
 */
function card_field_attach_form($entity_type, $entity, array &$form, array &$form_state, $langcode) {
  if ($entity_type == 'payment') {
    $payment = $entity;

    foreach (card_field_dependencies() as $name => $dependency) {
      // Show this field only when the selected payment method requires it.
      if ($dependency['field_hide_unnecessary'] && (!$payment->method || !isset($dependency['required_by'][$payment->method->controller->name]))) {
        $form[$dependency['field_name']]['#access'] = FALSE;
      }
    }
  }
}

/**
 * Implements hook_payment_validate().
 */
function card_payment_validate(Payment $payment, PaymentMethod $payment_method, $strict = TRUE) {
  if ($strict) {
    // See if this controller requires billing information.
    if (!empty($payment_method->controller->billing)) {
      // See if the payment has any billing information set.
      $information = card_extract($payment);
      foreach ($payment_method->controller->billing as $property) {
        if ($information[$property] === FALSE) {
          $dependencies = card_field_dependencies();
          throw new PaymentBillingValidationMissingInformationException(t('Missing %information billing information.', array(
            '%information' => $dependencies[$property]['title'],
          )));
        }
      }
    }
  }
}

/**
 * Return information about fields that are required for billing information.
 */
function card_field_dependencies() {
  static $dependencies = NULL;

  if (is_null($dependencies)) {
    $dependencies = array(
      'postal_address' => array(
        'field_type' => 'addressfield',
        'title' => t('Postal address'),
      ),
      'email_address' => array(
        'field_type' => 'email',
        'title' => t('Email address'),
      ),
    );
    foreach ($dependencies as $name => &$dependency) {
      // Get this dependency's field configuration.
      $dependency['field_name'] = variable_get('card_field_name_' . $name, FALSE);
      $dependency['field_hide_unnecessary'] = variable_get('card_field_hide_unnecessary_' . $name, FALSE);
      $dependency['field_hide_predefined'] = variable_get('card_field_hide_predefined_' . $name, FALSE);
      // Get all controllers that require this dependency.
      $dependency['required_by'] = array();
      foreach (payment_method_controller_load_multiple() as $controller) {
        if (!empty($controller->billing) && in_array($name, $controller->billing)) {
          $dependency['required_by'][$controller->name] = $controller->title;
        }
      }
    }
  }

  return $dependencies;
}

/**
 * Implements form build callback: the global configuration form.
 */
function card_form_configuration(array $form, array &$form_state) {
  $form['#submit'][] = 'card_form_configuration_submit';

  foreach (card_field_dependencies() as $name => $dependency) {
    // Find available field instances.
    $options = array();
    foreach (field_info_instances('payment', 'payment') as $instance) {
      $info = field_info_field($instance['field_name']);
      if ($info['type'] == $dependency['field_type']) {
        $options[$instance['field_name']] = $instance['label'];
      }
    }
    if ($options && !$dependency['required_by']) {
      $options[] = t('none');
    }

    $base = array(
      '#title' => t('Field'),
      '#description' => $dependency['required_by'] ? t('This field is required for %controller_titles.', array(
        '%controller_titles' => implode(', ', $dependency['required_by']),
      )) : '',
      '#required' => TRUE,
    );
    $form[$name] = array(
      '#type' => 'fieldset',
      '#title' => $dependency['title'],
    );
    $form[$name]['card_field_name_' . $name] = array(
      '#type' => 'radios',
      '#options' => $options,
      '#default_value' => $dependency['field_name'],
    ) + $base;
    if (!$options) {
      $form[$name]['card_field_name_' . $name]['#access'] = FALSE;
      $form[$name]['card_field_name_no_options' . $name] = array(
      '#type' => 'item',
      '#markup' => t('There are no <em>!field_type</em> fields available to use for billing information.', array(
        '!field_type' => $dependency['field_type'],
      )),
      ) + $base;
    }
    $form[$name]['card_field_hide_unnecessary_' . $name] = array(
      '#type' => 'checkbox',
      '#title' => t('Show this field only when the selected payment method requires it.'),
      '#default_value' => TRUE,
    );
  }

  return system_settings_form($form);
}

/**
 * Implements form submit callback for card_form_configuration().
 */
function card_form_configuration_submit(array $form, array &$form_state) {
  foreach (card_field_dependencies() as $name => $dependency) {
    if ($form_state['values']['card_field_name_' . $name]) {
      $instance = field_info_instance('payment', $form_state['values']['card_field_name_' . $name], 'payment');
      $instance['required'] = TRUE;
      if ($name == 'postal_address') {
        $instance['widget']['settings']['format_handlers'] =array(
          'address' => 'address',
          'organisation' => 'organisation',
          'name-full' => 'name-full',
        );
      }
      field_update_instance($instance);
    }
  }
}

/**
 * Extract billing information from a Payment object.
 *
 * @param Payment $payment
 *
 * @return array
 *   An array where keys are identical to the keys of the array returned by
 *   card_field_dependencies(). Values are the first value of the
 *   corresponding fields or FALSE if no field values could be found.
 */
function card_extract(Payment $payment) {
  $dependencies = card_field_dependencies();
  $information = array_fill_keys(array_keys($dependencies), FALSE);
  foreach ($dependencies as $name => $dependency) {
    if ($dependency['field_name']) {
      if ($items = field_get_items('payment', $payment, $dependency['field_name'])) {
        $information[$name] = $items[0];
      }
    }
  }

  return $information;
}

/**
 * Exception thrown if a payment misses required billing information.
 */
class PaymentBillingValidationMissingInformationException extends PaymentValidationException {}